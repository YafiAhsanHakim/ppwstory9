from django.test import TestCase, LiveServerTestCase
from django.test.client import Client
from django.urls import resolve
from django.apps import apps
from .views import index
from homepage.apps import HomepageConfig
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
class TestUnit(TestCase):

    def test_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_using_index_function(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_template_used(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_app(self):
        self.assertEqual(HomepageConfig.name, 'homepage')
        self.assertEqual(apps.get_app_config('homepage').name, 'homepage')

    def test_title_exists(self):
        response = Client().get('/')
        response_content = response.content.decode('utf-8')
        self.assertIn("Story 9", response_content)

class FunctionalTest(LiveServerTestCase):

    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(chrome_options=chrome_options, executable_path='chromedriver')

    def tearDown(self):
        self.browser.quit()

    def test_search_box_and_search_button(self):
        self.browser.get(self.live_server_url)
        time.sleep(2)

        search = self.browser.find_element_by_id('search')
        button = self.browser.find_element_by_id('searchButton')
        search.send_keys('harry')
        time.sleep(3)

        button.click()
        time.sleep(3)

        self.assertIn('Harry Potter', self.browser.page_source)
        time.sleep(2)

    def test_like_button(self):
        self.browser.get(self.live_server_url)
        time.sleep(2)

        search = self.browser.find_element_by_id('search')
        button = self.browser.find_element_by_id('searchButton')
        search.send_keys('harry')
        time.sleep(3)
 
        button.click()
        time.sleep(3)

        likeButton = self.browser.find_element_by_id('button0')
        likeButton.click()
        time.sleep(3)

        likes = self.browser.find_element_by_id('likes0').text
        self.assertEqual(likes, '1')

    def test_top_5_books(self):
        self.browser.get(self.live_server_url)
        time.sleep(2)

        search = self.browser.find_element_by_id('search')
        button = self.browser.find_element_by_id('searchButton')
        search.send_keys('harry')
        time.sleep(3)

        button.click()
        time.sleep(3)
        
        title = self.browser.find_element_by_id('title1').text
        likeButton1 = self.browser.find_element_by_id('button1')
        likeButton1.click()
        likeButton1.click()
        time.sleep(3)

        modalButton = self.browser.find_element_by_id('modalButton')
        modalButton.click()
        time.sleep(3)

        topBook = self.browser.find_element_by_id('topBook0').text
        self.assertEqual(title, topBook)




